const route = require("express").Router();
const uploadImage = require("../Middelwers/uploadImage");
const authentification = require("../Controllers/authentificationController");
//const check_auth = require("../Middelwers/check_authentification");
const passport = require("passport");
const authentificationController = require("../Controllers/authentificationController");
require("../Middelwers/passport_authentification").passport; //as strategy in paspport_authentificaion.js needs passport object

route.post(
  "/registreClient",
  uploadImage.single("image"),
  authentification.registreClient
);
//route.post("/registreClient", authentification.registreClient);

route.get("/verifyNow/:verificationcode", authentification.verifyEmail);
route.post("/login", authentification.login);
// 1 ere methode avec module jwt
//route.get("/profile", check_auth,authentification.profile);

//2 eme methode profile avec les modules passport et passport-jwt
route.get("/profile",passport.authenticate("jwt", { session: false }),authentification.profile);

route.get("/afficheclient", authentificationController.getClient);
route.get("/afficheclientById/:id", authentificationController.getclientById);
route.delete("/deleteclient/:id",authentification.deleteclient);
route.put("/upclient/:id",authentification.updateClient);
route.get("/forgetpassword", authentification.forgetpassword);
route.get("/resetpassword/:resetpassword",authentification.resetpassword);
route.get(
  "/refreshtoken",
  passport.authenticate("jwt", { session: false }),
  authentification.refrechToken
);
module.exports = route;
