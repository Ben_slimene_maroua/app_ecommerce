const express = require("express");
const orderController = require("../Controllers/orderController");
const passport = require("passport");
require("../Middelwers/passport_authentification").passport;
const route = require("express").Router();
route.post("/createorder", orderController.createOrder);
route.get("/afficheorder", orderController.getOrder);
route.get(
  "/orderbyclient",
  passport.authenticate("jwt", { session: false }),
  orderController.getOrderByClient
);
route.put("/updateorder/:id", orderController.updateorder);
route.delete("/deleteorder/:id", orderController.deleteorder);
module.exports = route;
