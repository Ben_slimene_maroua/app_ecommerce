const express=require ("express");
const providercontroller= require("../Controllers/providerController");
const route= require("express").Router();

route.post("/addprovider",providercontroller.addprovider);
route.get("/getprovider",providercontroller.getprovider);


module.exports= route;