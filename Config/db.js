const { connect } = require("mongoose");
const { success, error } = require("consola");
const Db = process.env.APP_DB;
const connectDb = async () => {
  try {
    await connect(Db);
    success({
      message: `Successfully connected with the Database \n${Db}`,
      badge: true,
    });
  } catch (err) {
    error({
      message: `Unable to connect with Database \n${err}`,
      badge: true,
    });
    connectDb();
  }
};
module.exports = connectDb();