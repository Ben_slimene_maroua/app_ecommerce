const {Strategy,ExtractJwt}= require("passport-jwt");// npm i passport passport-jwt
const passport=require("passport");
const SECRET= process.env.APP_SECRET;
const User=require("../Models/User");

var options={
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: SECRET,
};
passport.use(
    new Strategy(options,async ({id}, done)=>{
        try {
           const user= await User.findById(id).populate("orders");
           if(!user){
            throw new Error('user not found'); //throw gere les exceptions, elle te permet d utiliser la methode Error
             // on peut faire ca: done(null,"User not found")  
           } 
           done(null, user)
        } catch (error) {
           done(null,error.message); 
        }
    })
);