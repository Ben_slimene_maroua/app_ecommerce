const Category = require("../Models/Category");

module.exports = {
  createCategory: async (req, res) => {
    const newCategory = new Category(req.body);
    const category = await Category.create(newCategory);
    try {
      res.status(201).json({
        message: "categorie ajouté",
        data: category,
      });
    } catch (error) {
      res.status(500).json({
        message: message.error,
      });
    }
  },

  getCategory: async (req, res) => {
    try {
      const category = await Category.find({}).populate("subCategories");
      res.status(201).json({
        message: "voila votre liste des categories",
        data: category,
      });
    } catch (error) {
      res.status(500).json({
        message: message.error,
      });
    }
  },
getCategoryById: async(req,res)=>{
try {
    const category = await Category.findById({_id:req.params.id}).populate("subCategories");
   ;
      res.status(201).json({
        message: "voila votre categorie",
        data: category
      });
} catch (error) {
    res.status(500).json({
        message: message.error,
    });
}
},
getCategoryByName:async (req,res)=>{
try {
    const category= await Category.find({nom:req.query.nom});
    res.status(201).json({
        message:"voila votre categorie",
        data: category,
    });
    
} catch (error) {
    res.status(500).json({
        message: message.error,
    });
}
},
updateCategory: async (req,res)=>{
try {
    await Category.updateOne({_id:req.params.id}, req.body);
    res.status(201).json({
        message:"votre categorie est modifié",
    })
} catch (error) {
    res.status(500).json({
        message:message.error,
    });
    
}
},
deletecategory: async (req,res)=>{
    try {
        await Category.deleteOne({_id:req.params.id});
        res.status(201).json({
            message: "votre categorie est supprimé",
        });

    } catch (error) {
        res.status(500).json({
            message: message.error,
        })
    }
}
};
