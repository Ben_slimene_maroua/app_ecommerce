const provider= require('../Models/Provider');
const product=require('../Models/Product');
const Provider = require('../Models/Provider');
module.exports={
    addprovider:async(req,res)=>{
        try {
            const newprovider= new Provider(req.body);
            const provider= await newprovider.save();

            res.status(201).json({
                message:("fournisseur ajouté"),
                data: provider,
            })
        } catch (error) {
            res.status(500).json({
                message:error.message,
            });
        }
    },
    getprovider: async(req,res)=>{
        try {
            const provider= await Provider.find({});
            res.status(201).json({
                message:('voila votre fournisseur'),
                data: provider,
            })
        } catch (error) {
            res.status(500).json({
                message: error.message,
            })
        }
    }
}