const Product = require("../Models/Product");
const SubCategory = require("../Models/SubCategory");
module.exports = {
  createProduct: async (req, res) => {
    try {
    req.body["galleries"] =
       req.files.length <= 0
          ? []
         : req.files.map(function (file) {
            return {
               name: file.filename,
                 description: "this picture of product",
              };
            });
   
      const newProduct = new Product(req.body);
      const product = await newProduct.save();

      await SubCategory.findByIdAndUpdate(req.body.subcat, {
        $push: { products: product },
      }); // on fait une recherche du id subcat qui appartient au nouvel produit(instance),une fois trouvé npoushiw feha produit dans l attribut products qui existe fi schema subcategory

      res.status(201).json({
        message: "produit ajouté",
        data: product,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },

  getProduct: async (req, res) => {
    try {
      const listProduct = await Product.find({}).populate({path:'subcat',populate:{path:'category'}}).populate("provider");
      res.status(200).json({
        message: "voila la liste des produits",
        data: listProduct,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  getProductById: async (req, res) => {
    try {
      const getproduct = await Product.findById({
        _id: req.params.id,
      }).populate("subcat");
      res.status(200).json({
        message: "voila votre produit",
        data: getproduct,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  updateProduct: async (req, res) => {
    try {
      await Product.updateOne({ _id: req.params.id }, req.body);
      res.status(200).json({
        message: "votre produit est modifié",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  deleteProduct: async (req, res) => {
    try {
      const product =await Product.findById({_id:req.params.id});
      await SubCategory.findByIdAndUpdate(product.subcat,{
        $pull: { products: product._id },
      });
      await Product.deleteOne({ _id: req.params.id });
      res.status(200).json({
        message: "votre produit est supprimé",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
};
