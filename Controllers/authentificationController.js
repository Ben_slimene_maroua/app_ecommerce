const Client = require("../Models/Client");
const User = require("../Models/User");
const bcrypt = require("bcrypt"); //pour le cryptage des passwords

const { randomBytes } = require("crypto");
const { join } = require("path");
const jwt = require("jsonwebtoken"); //necessaire pour la creation d un jeton du session
const DOMAIN = process.env.APP_DOMAIN;
const nodemailer = require("nodemailer"); //assure l envoie des emails
const SECRET = process.env.APP_SECRET;
var RefrechTokens=[];
//transporteur du service qui te permet d envoyer email
const transporteur = nodemailer.createTransport({
  service: "gmail",
  auth: { user: process.env.APP_USER, pass: process.env.APP_PASS },
});

module.exports = {
  registreClient: async (req, res) => {
    try {
     req.body["image"] = req.file.filename; //insertion d une seule image
      const password = bcrypt.hashSync(req.body.password, 10); //cryptage du password

      const newClient = new Client({
        ...req.body,
        password,
        verificationcode: randomBytes(6).toString("hex"),
      });
      const regClient = await Client.create(newClient);
      res.status(201).json({
        message: "Votre compte est crée veuillez verifier votre email",
      });

      transporteur.sendMail(
        {
          to: regClient.email,
          subject: "bienvenue" + regClient.nom,
          text: "Bonjour monsieur",
          html: `
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome email</title>
</head>
<body>
  <h2 >Hello   ${regClient.nom}</h2>  
  <p>we are glad to have you on board at  ${regClient.email}</p>
  <a href="${DOMAIN}/verifyNow/${regClient.verificationcode}">Verify email</a>
</body>
</html>`,
        },
        function (err, info) {
          if (err) {
            console.log("error: " + err.message);
          } else {
            console.log("email sended:" + info.response);
          }
        }
      );
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },

getClient: async(req,res)=>{
  try {
    const regclient= await Client.find({}).populate("orders");
    res.status(201).json({
      message:"ceci la liste des clients",
      data: regclient,
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
},


  verifyEmail: async (req, res) => {
    try {
      const verificationcode = req.params.verificationcode;
      const user = await User.findOne({ verificationcode });
      user.verifier = true;
      user.verificationcode = undefined;

      user.save();
      res.sendFile(join(__dirname, "../Templates/verificationsucess.html"));
    } catch (error) {
      res.sendFile(join(__dirname, "../Templates/errors.html"));
    }
  },
  login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(500).json({
          message: "email non validé",
        });
      }
      const passwordcompare =bcrypt.compareSync(password, user.password);
      if (!passwordcompare) {
        return res.status(500).json({
          message: "password non validé",
        });
      }
      if (user.verifier === false) {
        res.status(500).json({ message: "your account is not defind" });
      } else {
        const token = jwt.sign({ id: user._id, user: user }, SECRET, {
          expiresIn: "7 days",
        });
        const refrechToken = jwt.sign({ id: user._id }, SECRET, {
          expiresIn: "24h",
        });
        RefrechTokens[refrechToken] = user._id;
        const result = {
          email: user.email,
          user: user,
          token: token,
          expiresIn: 1,
          refrechToken: refrechToken,
        };
        res.status(200).json({
          message: "login validé",
          ...result,
        });
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },

  profile: async (req, res) => {
    try {
      const user = req.user;
      res.status(200).json({ message: "you are authentify", data: user });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  deleteclient: async (req,res)=>{
    try {
        await Client.deleteOne({_id:req.params.id});
        res.status(201).json({
            message: "votre client est supprimé",
        });

    } catch (error) {
        res.status(500).json({
            message: message.error,
        })
    }
},
getclientById: async(req,res)=>{
  try {
      const client = await Client.findById({_id:req.params.id});
     ;
        res.status(201).json({
          message: "voila votre client",
          data: client
        });
  } catch (error) {
      res.status(500).json({
          message: message.error,
      });
  }
  },
  updateClient: async (req,res)=>{
    try {
        await Client.updateOne({_id:req.params.id}, req.body);
        res.status(201).json({
            message:"votre client est modifié",
        })
    } catch (error) {
        res.status(500).json({
            message:message.error,
        });
        
    }
    },
 refrechToken : async (req, res) => {
      try {
        const refrechToken = req.body.refrechToken;
      
        if (refrechToken in RefrechTokens) {
          const token = jwt.sign({ id: req.user._id }, SECRET, { expiresIn: "7h" });
          const refrechToken = jwt.sign({ id: req.user._id }, SECRET, {
            expiresIn: "24h",
          });
          RefrechTokens[refrechToken] = req.user._id;
          res.status(200).json({
            accesstoken: token,
            refrechToken: refrechToken,
          });
        } else {
          res.status(404).json({
            status: 404,
            message: "refresh token not exist",
          });
        }
      } catch (error) {
        res.status(404).json({
          status: 404,
          message: error.message,
        });
      }
    },

forgetpassword : async (req, res) => {
      try {
        const user = await Client.findOne({ email: req.body.email });
        console.log(user);
        if (!user) {
          return res.status(500).json({ message: "Your email does not exist" });
        }
        const token = jwt.sign({ id: user._id }, SECRET, {
          expiresIn: "2h",
        });
        console.log("resettttt :", token);

        transporteur.sendMail(
          {
            to: user.email,
            subject: "Forget Password",
            text: "Bonjour MR",
            html: `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Welcome Email</title>
            </head>
            <body>
                <h2>
                    Hello ${user.nom}
                </h2>
                <p>We are glad to have you on bord at ${user.email}</p>
                <a href="${DOMAIN}/resetpasswordd/${token}">Reset Password</a>
            </body>
            </html>`,
          },
           //fonction pour afficher msg lors de la creation de client lors de lenvoi du email
      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.res);
        }
      }
    );
    return res.status(200).json({ message: "email send" });
  } catch (error) {}
},
resetpassword : async (req, res) => {
  try {
    const resetpassword = req.params.resetpassword;
    jwt.verify(resetpassword, SECRET, async (err) => {
      if (err) {
        return res.json({ error: "incorrect token or it is expired" });
      }
      const user = await User.findOne({ token : resetpassword });
      const password =bcrypt.hashSync(req.body.newpassword, 10);
      user.password = password;
      user.save();
      res.status(200).json({ message: "password modified" });
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}
};
