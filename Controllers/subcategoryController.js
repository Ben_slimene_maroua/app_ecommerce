const SubCategory = require("../Models/SubCategory");
const Category = require("../Models/Category");
module.exports = {
  createSubCategory: async (req, res) => {
    try {
      const newSubCat = new SubCategory(req.body);
      const subcategory = await newSubCat.save();

      await Category.findByIdAndUpdate(req.body.category, { $push: { subCategories: subcategory },
      });
      res.status(201).json({
        message: "Sub categorie creé",
        data: subcategory,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  getsubCategory: async (req, res) => {
    try {
      const subcat = await SubCategory.find({}).populate("category").populate("products");
      res.status(201).json({
        message: "voila la liste des sub categories",
        data: subcat,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  getsubCatByID: async (req, res) => {
    try {
      const subcat = await SubCategory.findById({
        _id: req.params.id,
      }); // .populate("id_category")
      res.status(201).json({
        message: "voila votre categorie",
        data: subcat,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  getsubCatByName: async (req, res) => {
    try {
      const subcat = await SubCategory.find({ nom: req.query.nom }).populate(
        "id_category"
      );
      res.status(201).json({
        message: "voila votre sub  categorie",
        data: subcat,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  updateSubCat: async (req, res) => {
    try {
      await SubCategory.updateOne({ _id: req.params.id }, req.body);
      res.status(201).json({
        message: "votre sub categorie est modifié",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  deleteSubCat: async (req, res) => {
    try {
      const subcat= await SubCategory.findById({_id:req.params.id});
      await Category.findByIdAndUpdate(subcat.category, { $pull: { subCategories: subcat._id },});
      await SubCategory.deleteOne({ _id: req.params.id });

      res.status(201).json({
        message: "votre sub categorie est supprimé",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
};
