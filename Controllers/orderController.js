const Order = require("../Models/Order");
const Client = require("../Models/Client");
module.exports = {
  createOrder: async (req, res) => {
    try {
     
      const neworder = new Order(req.body);
      const order = await neworder.save();
      await Client.findByIdAndUpdate(req.body.client, {
        $push: {orders: order },
      });
      res.status(201).json({
        message: "commande ajouté",
        data: order,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  getOrder: async (req, res) => {
    try {
      const order = await Order.find({});
      res.status(201).json({
        message: "voici votre commande",
        data: order,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
 /* dedié pour admin
 getOrderByClient: async (req, res) => {
    try {
      const orderbyclient = await Order.find({ client: req.query.client });
      res.status(201).json({
        message: "voila votre order",
        data: orderbyclient,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },*/
  getOrderByClient: async (req, res) => {
    try {
      const orderbyclient = req.user.orders;
      res.status(201).json({
        message: "voila votre order",
        data: orderbyclient,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  updateorder: async (req, res) => {
    try {
      req.body["itemorders"] = {
        product: req.body.product,
        qte: req.body.qte,
        price: req.body.price,
      };
      await Order.updateOne({ _id: req.params.id }, req.body);
      res.status(201).json({
        message: "votre commande est modifiée",
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
  deleteorder: async (req, res) => {
    try {
       const order= await Order.findById({_id:req.params.id});

        await Client.findByIdAndUpdate(order.client, {
            $pull: { orders: order._id },
          });
        await Order.deleteOne({_id:req.params.id});
        res.status(201).json({
            message:"votre commande est supprimé",
        });

    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
};
