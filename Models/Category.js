const mongoose = require("mongoose");
const schemaCategory = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    subCategories:[
      {
        type:mongoose.Types.ObjectId,
        ref:'SubCategory',
      }
    ]
  },
 // { timestamps: true }
);
module.exports = mongoose.model("Category", schemaCategory);
