const mongoose= require("mongoose");
const schemaitemorder= new mongoose.Schema({
    product:[     
            {
              type: mongoose.Types.ObjectId,
              ref: "Product",
            },
          ],
          quantite:{
        type:Number,
        required:true,
    } ,   
    prix:{
        type:Number,
        required:true,
    }  , 

});
const schemaOrder= new mongoose.Schema({
    client:{
        type:mongoose.Types.ObjectId,
        ref:'Client',
    },
    quantite:{
        type: Number,
        required:true,
    },
    prix:{
        type: Number,
        required: true,
    },
    payer:{
        type:Boolean,
        default:false,
    },
    itemorders:[schemaitemorder],
});
module.exports= mongoose.model("Order",schemaOrder);