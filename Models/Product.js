const mongoose = require("mongoose");
const GallerySchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  description: {
    type: String,
    trim: true,
    required: true,
  },
});
const schemaProduct = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
    },
    description: {
        type: String,
        required: true,
      },
    prix: {
      type: Number,
      required: true,
    },
    quantite: {
        type: Number,
        required: true,
      },
      reference: {
        type: String,
        required: true,
      },
      color:{
        type:String,
      },
    galleries: [GallerySchema],
   
    subcat: {
      type: mongoose.Types.ObjectId,
      ref: "SubCategory",
    },
    provider:{
      type: mongoose.Types.ObjectId,
      ref: "Provider"
    }
  },
 //{ timestamps: true }
);
module.exports = mongoose.model("Product", schemaProduct);
