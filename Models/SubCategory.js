const mongoose = require("mongoose");
const schemaSubCategory = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    category: {
      type: mongoose.Types.ObjectId,
      ref: "Category",
      required:true,
     
    },
    products: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Product",
      },
    ],
  },
  //{ timestamps: true }
);
module.exports = mongoose.model("SubCategory", schemaSubCategory);
