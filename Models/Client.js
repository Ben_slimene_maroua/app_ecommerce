const mongoose = require("mongoose");
const User=require("./User");

const schemaClient = new mongoose.Schema(
  {
   nom: {
      type: String,
      required: true, 
      trim: true,
      
    },
  prenom: {
      type: String,
      required: true, 
    },
   adresse: {
        type: String,
        required: true, 
      },
      image:{
        type: String
      },
   
  orders:{
    type:mongoose.Types.ObjectId,
    ref:"Order",
  }, },
  { timestamps: true } 
);
//client herite du l user
module.exports = User.discriminator("Client", schemaClient);