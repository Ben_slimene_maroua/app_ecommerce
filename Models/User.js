const mongoose = require("mongoose");
const schemaUser = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
    },
    verifier: {
      type: Boolean,
      default: false,
    },
    verificationcode: {
      type: String,
    },
    resetpassword:{
      type:String,
      required:false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", schemaUser);
