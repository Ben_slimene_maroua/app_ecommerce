const express = require("express");
const cors = require("cors");
require("dotenv").config();
const { success, error } = require("consola");
const dbecommerce = require("./Config/db");
const port = process.env.APP_PORT || 5000;
const DOMAIN = process.env.APP_DOMAIN;
const passport = require("passport");
const authRoute = require("./Routes/authRoute");
const categoryRoute = require("./Routes/categoryRoute");
const subcatRoute = require("./Routes/subcatRoute");
const productRoute= require("./Routes/productRoute");
const orderRoute=require("./Routes/orderRoute");
const providerRoute= require("./Routes/providerRoute")

const app = express();

//les middelwares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(passport.initialize());

app.use("/", authRoute);
app.use("/category", categoryRoute);
app.use("/subcategory", subcatRoute);
app.use("/product",productRoute);
app.use("/order",orderRoute);
app.use("/provider",providerRoute);

//pour afficher l image
app.get("/getfile/:image",function(req,res){
  res.sendFile(__dirname+'/storages/'+req.params.image)
})

app.listen(port, async () => {
  try {
    success({
      message: `Server started on PORT ${port} ` + `URL : ${DOMAIN}`,
      badge: true,
    });
  } catch (err) {
    error({ message: "error with server", badge: true });
  }
});
